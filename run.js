const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function main (params) {
  console.log(params);
  var command = {};
  command.input = 'ibmcloud login --apikey ' + params.apikey + ' -r eu-de'
  + ' && ibmcloud target -g ' + params.resourcegroup + ' -o ' + params.org + ' -s ' + params.space
  + ' && ibmcloud dev pipeline-run ' + params.pipelineId;
	

  const { stdout, stderr } = await exec(command.input);
    if (stderr) {
      console.error(`error: ${stderr}`);
      command.status = 'error';
      command.output = stderr;
    }else{
      console.log(`Output ${stdout}`);
      command.status = 'success';
      command.output = stdout;
    }
  return {result: command};
}
