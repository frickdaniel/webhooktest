# Dockerfile for Fressnapf
FROM ibmfunctions/action-nodejs-v10

# Install IBM Cloud CLI
RUN curl -sL https://clis.ng.bluemix.net/install/linux | bash  && ibmcloud config --check-version=false

# Disable Version Update
RUN ibmcloud config --check-version=false

# Install dev tools
RUN ibmcloud plugin install dev

# Copy run script
COPY run.js .
